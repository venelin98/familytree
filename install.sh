#!/bin/sh
set -e

# Install location
PREFIX=/usr/local

# Compilation options
language='
	-std=c++17
	-fno-rtti
	-fno-exceptions'

optimizations='
	-Ofast
	-flto
	-march=native'

warnings='
	-Wall
	-Wextra
	-Wno-implicit-fallthrough
	-Wno-multichar'
#	-Wpedantic' #-fanalyzer

other='-fno-fat-lto-objects'

# Compile
cd src
g++ -o tft $language $optimizations $warnings $other -DNDEBUG -pipe -s *.cpp -lncursesw
#-static -lncursesw -ltinfo

# Install
chmod 755 tft
mkdir -p ${PREFIX}/bin
mv tft ${PREFIX}/bin
