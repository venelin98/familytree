#pragma once

enum
{
	SELECT_LEFT  = 'h',
	SELECT_DOWN  = 'j',
	SELECT_UP    = 'k',
	SELECT_RIGHT = 'l',

	DISPLAY_INFO = 'i',
	SELECT       = ' ',			/* spacebar */
	CLOSE        = 'q',

	MOVE_LEFT  = KEY_LEFT,		/* arrows */
	MOVE_DOWN  = KEY_DOWN,
	MOVE_UP    = KEY_UP,
	MOVE_RIGHT = KEY_RIGHT

};
