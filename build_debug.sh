#!/bin/sh

language='
	-std=c++17
	-fno-rtti
	-fno-exceptions'

warnings='
	-Wall
	-Wextra
	-Wno-implicit-fallthrough
	-Wno-multichar'

cd src
g++ -o ../tft_debug $language -g -O0 -march=native $warnings -pipe *.cpp -lncursesw
