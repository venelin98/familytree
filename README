tft - Terminal Family Tree
==========================
tft is an ncurses program that can create and display family trees on the terminal.

Webpage
-------
https://vene.volconst.com/en/tft

Dependecies
-----------
ncursesw library and headers (libncurses-dev on Debian/Ubuntu)

Installation
------------
Make sure you satisfy the dependencies and execute:
	git clone https://gitlab.com/venelin98/tft.git
	cd tft
	sudo ./install.sh

Creating trees
--------------
Trees are created in plain text files following the tft file format.
Spaces are not important but new lines are considered as separators.

Adding a member
---------------
A new line begining with a name is considered a member definition, the name can include spaces.

After the name the sex can be specified with a comma followed by either M or F.
If not given male is assumed, unless a spouse is present in which case the opposite sex assumed.

For example:
	Adam, M
	Declares member Adam that is male.

Adding member information
-------------------------
In the lines following a member declaration, various info can be added:
s: - Spouses, must be followed by comma separated names of already declared members, "^" can be given instead in which case the last declared person is used
p: - Parents, must be followed by 1 or 2 names separated by a comma, "-" can be given instead in which case the previous parents are used
b: - Birthday in the format yyyy.mm.dd, day and month can be ommited
d: - Death date

Duplicate names
---------------
A number after a name is used to disambiguate when multiple people have identical names e.g.:
	"Adam 0" is the first added Adam, "Adam 1" is the second

Viewing trees
-------------
After creating a tree file you can view it using tft:
	tft my_tree.tft

Once the tree is displayed you can navigate it using:
	arrows - move the camera
	h, j, k, l - move around the tree
	Enter, Space - display the selected person's tree
	+ - show full names
	- - show first names only
	q - close the tree

УПЪТВАНЕ НА БЪЛГАРСКИ:
https://vene.volconst.com/bg/tft
