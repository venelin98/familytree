Give error when wrongly indexed person declaration

findRelative

findRelation - extend

Person info

man page

types - screenCoord...

tft descriton - multiple trees, why its better, features, ^
tft merge trees relation

Visualization:
- more granular zooming
- snap to selection/scroll with selection
- centralize when changing focus
- horizontal navigation between cousins
- color on different consoles
- screen resize
- empty tree
- half children and main tree
- aproximate birth day

Compilation
- static and dynamic ncurses

?
- info bar
- distinct regions of a tree
- same person can be displayed twice
- coordinate types for large trees

Refactoring
- comment style /*
- check return codes - setlocale,
- focused -> root
- tree::relations_ to std::set

test
- no Adam

readelf -a /usr/lib/x86_64-linux-gnu/libncursesw.a | grep lto
