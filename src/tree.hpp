#pragma once

#include <climits>
#include <vector>
#include "person.hpp"

using person_id = U32;
enum: person_id {
	Nobody = UINT32_MAX
};

enum RelType : U8
{
	None = 0,
	Parent,
	Child,
	Sibling,
	HalfSibling,
	Spouse,
	ExSpouse,

	Other
};

struct Relation
{
	person_id id;
	RelType type;
};

class Tree
{
public:
	Tree() = default;
	template <class Str>
	explicit Tree(Str&& Name) : treeName_(std::forward<Str>(Name)) {}

	Tree operator+ (const Tree& other) const;
	Tree operator- (const Tree& other) const; /* todo */
	Tree& operator+= (const Tree& other);
	Tree& operator-= (const Tree& other);

	U32 size();
	bool empty();
	person_id last();

	// Get person by id
	const Person& operator[](person_id) const;

	std::vector<person_id> findId(const std::string& name) const; // todo merge the 2 findId
	person_id findId(const std::string& name, Sex) const;
	person_id findRootAncestor(person_id) const;
	person_id findOldestAncestor(person_id, Sex) const;//

	// Oldest common ancestor ID, a person is considerd an ancestor of himself
	person_id findCommonAncestor(person_id first, person_id second) const; /* needs to be fixed, never return nobody */
	RelType   findRelation(person_id first, person_id second) const;
	const std::vector<Relation>& findRelations(person_id) const;
	person_id findParent(person_id child, Sex parentSex) const;
	person_id findSpouse(person_id person) const;
	std::vector<person_id> findChildren(person_id person) const;
	std::vector<person_id> findChildren(person_id person, person_id exclude) const; // exclude the child with the given ID

	void addPerson(const char* name, Sex sex, person_id father=Nobody, person_id mother=Nobody, EventTime birth={}, EventTime death={});
	// Return weather it succeeded or not
	bool addRelation(person_id first, RelType, person_id second, bool update=true);

	void removePerson(person_id id);   //Removes a member and the last one takes his ID
	void removeRelation(person_id first, person_id second);

private:
	// recursion to find common ancestor
	void commonAncestorRec(person_id, std::vector<U8> &visited)const;

	void addRelOneSide(person_id first, RelType, person_id second);
	/* update a person's relations after adding a relation
	   (example adds siblings after parents are added) */
	void updateRels(person_id);

	std::string treeName_;
	std::vector<Person> people_;  //The data for each member
	std::vector<std::vector<Relation>> relations_;  //The relatives of each member
};
