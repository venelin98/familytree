#pragma once

#include <string>
#include <fstream>
#include <ncurses.h>
#include "common/basicTypes.h"

enum ZoomLevel: U8;

enum: I16 { UNKNOWN_DATE=0  };

struct EventTime
{

	EventTime(I16 Year=UNKNOWN_DATE, U8 Month=UNKNOWN_DATE, U16 Day=UNKNOWN_DATE);
	void print(WINDOW*) const;

	I16 year;		//The year in relation to Chritst's birth -  1 (AD) is the year He was born, -1 (BC) is the previous year. There is no year 0
	U8  month;
	U16 day;
};

enum Sex: U8
{
	M = 0,
	F,
	UNKNOWN_SEX
};

enum BoxColors: U8 {
	COLOR_MALE = 1,
	COLOR_FEMALE,
	COLOR_SELECTED
};

enum DisplayDimensons: U8 {
	// BOX_HEIGHT = 5,
	LINK_HEIGHT = 3
};

class Person
{
public:
	Person() = default;
	Person(const char* aName, Sex=M, EventTime aBirth={}, EventTime aDeath={});

	/* Are the two very likely the same person based on sex, name, dates */
	bool likelySame(const Person& other) const;

	/* Display the person's info on the screen */
	void displayInfo() const;

	/* Draw a Person's box on the terminal, as a part of the focused tree
	   drawY,drawX - top left coordinates where to draw (negative if outside the screen)
	   partOfMain - is the person a decendant of the current root
	   hasSpouse, hasSpouse, hasChildren - wheather the person has them on display
	   ZoomLevel - how the box will be drawn
	   returns the width of the box*/
	U16 drawAsFocused(I16 drawY, I16 drawX, bool selected, bool hasSpouse,
			  bool hasChildren, ZoomLevel) const;

	/* Draw a Person's box on the terminal, as a spouse of someone in the focused tree
	   drawY,drawX - top left coordinates where to draw (negative if outside the screen)
	   hasTree     - weather the spouse has a tree that can be displayed
	   ZoomLevel   - how the box will be drawn
	   returns the width of the box*/
	U16 drawAsSpouse(I16 drawY, I16 drawX, bool selected, bool hasTree, ZoomLevel) const;

	std::string name;
	Sex sex;
	EventTime birth;
	EventTime death;

private:
	U16 draw(I16 drawY, I16 drawX, bool selected, ZoomLevel,
		 U32 topleft, U32 topright, U32 botleft, U32 botright) const;
};

U16 person_box_height(ZoomLevel);
