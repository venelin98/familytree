#include <iostream>
#include <string_view>
#include <assert.h>
#include <ctype.h>
#include <ncurses.h>

#include "person.hpp"
#include "utils.hpp"
#include "ui.hpp"

using namespace std;

EventTime::EventTime(I16 Year, U8 Month, U16 Day)
	: year(Year)
	, month(Month)
	, day(Day)
{
	assert(month <= 12 && day <= 31 && "Invalid EventTime data");
}

void EventTime::print(WINDOW* info_tab) const
{
	if (day != UNKNOWN_DATE)
		wprintw(info_tab, "%d", day);
	else
		waddstr(info_tab, "??");
	waddch(info_tab, '.');

	if (month != UNKNOWN_DATE)
		wprintw(info_tab, "%d", month);
	else
		waddstr(info_tab, "??");
	waddch(info_tab, '.');

	if (year != UNKNOWN_DATE)
		wprintw(info_tab, "%d", year);
	else
		waddstr(info_tab, "????");
	waddch(info_tab, '\n');
}


Person::Person(const char* aName, Sex aSex, EventTime aBirth, EventTime aDeath)
	: name(aName)
	, sex(aSex)
	, birth(aBirth)
	, death(aDeath)
{
}

static string_view nameWithoutFamily(const string& name)
{
	U32 fam_start = name.rfind(' ') ;
	string_view name_no_fam;

	if(fam_start != string::npos)
		name_no_fam = string_view(name.data(), name.size() - fam_start);
	else
		name_no_fam = string_view(name);

	return name_no_fam;
}

bool Person::likelySame(const Person& other)const
{
	bool dates_pass = (birth.day == other.birth.day || birth.day == UNKNOWN_DATE || other.birth.day == UNKNOWN_DATE) &&
		(birth.month == other.birth.month || birth.month == UNKNOWN_DATE || other.birth.month == UNKNOWN_DATE) &&
		(birth.year == other.birth.year || birth.year == UNKNOWN_DATE || other.birth.year == UNKNOWN_DATE);

	bool names_pass;
	if(sex == M)
	{
		names_pass = name == other.name;
	}
	else
	{
		string_view name_no_fam = nameWithoutFamily(name);
		string_view other_name_no_fam = nameWithoutFamily(other.name);

		names_pass = name_no_fam == other_name_no_fam;
	}

	return sex == other.sex && names_pass && dates_pass;
}

void Person::displayInfo()const
{
	AutoWin info_tab( newwin(LINES, COLS, 0, 0) );

	waddstr(info_tab, name.data());
	waddch(info_tab,'\n');

	waddstr(info_tab, "Birth: ");
	birth.print(info_tab);

	waddstr(info_tab, "Death: ");
	death.print(info_tab);

	mvwaddstr(info_tab, LINES-1, 0, "Press any key to return.");

	wrefresh(info_tab);
	getch();					/* wait press */

	werase(info_tab);
	wrefresh(info_tab);			/* todo: dont update */
}


static U16 Utf8SymbolsCount(const string& str)
{
	U16 num = 0;
	for(char c: str)
		num += ((c & 0xC0) != 0x80); // bytes that start with 0 are single, 11 are multiple, 10 are sucessors

	return num;
}

U16 Person::drawAsFocused(const I16 drawY, const I16 drawX, bool selected, bool hasSpouse,
			  bool hasKids, ZoomLevel zoom) const
{
	auto topright = hasSpouse ? ACS_TTEE : ACS_URCORNER;
	auto botleft = hasKids ? ACS_LTEE : ACS_LLCORNER;

	return draw(drawY, drawX, selected, zoom,
		    ACS_LTEE, topright,
		    botleft,  ACS_LRCORNER );
}

U16 Person::drawAsSpouse(I16 drawY, I16 drawX, bool selected, bool hasTree, ZoomLevel zoom) const
{
	auto topright = hasTree ? '@' : ACS_URCORNER;
	return draw(drawY, drawX, selected, zoom,
		    ACS_TTEE,     topright,
		    ACS_LLCORNER, ACS_LRCORNER );
}

U16 Person::draw(I16 drawY, I16 drawX, bool selected, ZoomLevel zoom,
		 U32 topleft, U32 topright, U32 botleft, U32 botright) const
{
	/* pad width */
	U16 width;
	// pad height
	U16 height;

	string display_name = name;
	switch(zoom)
	{
	case ZoomLevel::FIRST_NAME:
	{
		size_t space = display_name.find(' ');
		if(space != string::npos)
		{
			display_name.erase(space);
		}
	}
	case ZoomLevel::ALL_NAMES:
		width = Utf8SymbolsCount(display_name) + 4;
		height = 5;
		break;
	case ZoomLevel::NO_NAME:
		width = 2;
		height = 2;
		break;
	}

	if(willBeVisible(drawY, drawX, height, width))
	{
		AutoWin pad( newpad(height, width) );

		auto color_pair = COLOR_PAIR( selected ? COLOR_SELECTED
					      : (sex==M ? COLOR_MALE : COLOR_FEMALE) );

		wbkgd(pad, color_pair);
		/* wattron(pad, color_pair); */

		wborder(pad, ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE,
			topleft,  topright,
			botleft, botright);

		mvwaddstr(pad, 2, 2, display_name.data());

		/* wattroff(pad, color_pair); */

		drawpad(pad, drawY, drawX);
	}
	return width;

}

U16 person_box_height(ZoomLevel z)
{
	switch(z)
	{
	case ZoomLevel::ALL_NAMES:
	case ZoomLevel::FIRST_NAME:
		return 5;
	case ZoomLevel::NO_NAME:
		return 2;
	}
}
