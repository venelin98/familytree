#include "ui.hpp"
#include "tree.hpp"
#include "utils.hpp"
#include "assert.h"
#include "../config.h"		// user's configuration

using std::vector;

namespace
{
	// State
	person_id focused_;     // the tree was draw based on this person
	person_id selected_;	// the currently selected selected
	ZoomLevel zoom_;	// how much info to show for each selected

	// todo: figure out what to do with these constants
	const U8 dist_between_ = 1;		/* distance between people */
	const U8 spouse_dist_  = 0;		/* distance between spouses */

	void drawTree(const Tree&, const I16 offsetY, const I16 offsetX);
	/* draw a line of people starting from person with id
	   return the farthest X on the screen where it will be drawn */
	I16 drawTreeLine(const Tree&, person_id, I16 drawY, I16 drawX);


	void displayHelp();
	person_id selectLeft(const Tree&, person_id selected);
	person_id selectDown(const Tree&, person_id selected);
	person_id selectUp(const Tree&, person_id selected);
	person_id selectRight(const Tree&, person_id selected);
	person_id selectLeftSibSpouse(const Tree&, person_id selected); /* todo: split? */
	person_id selectRightSib(const Tree&, person_id selected);
}

ZoomLevel operator++(ZoomLevel& z)
{
	z = (ZoomLevel)(z + 1);
	return z;
}

ZoomLevel operator--(ZoomLevel& z)
{
	z = (ZoomLevel)(z - 1);
	return z;
}

void displayTree(const Tree& t)
{
	// 0 is the first person in the tree
	focused_ = 0;
	selected_ = 0;
	zoom_ = ZoomLevel::FIRST_NAME;

	// offset of the tree in relation to the top left corner of the screen
	I16 offsetY = 0, offsetX = 0;

	bool to_draw = true;
	I32 input;
	do
	{
		if(to_draw)
			drawTree(t, offsetY, offsetX);
		else
			to_draw = true;

		input = getch();
		switch(input)
		{
		case KEY_F(1):
			displayHelp();
			break;
		case SELECT:
		case '\n':
			focused_ = t.findRootAncestor(selected_);
			offsetX = 0;
			offsetY = 0;
			break;
		case MOVE_LEFT:
			++offsetX;
			break;
		case MOVE_RIGHT:
			--offsetX;
			break;
		case MOVE_DOWN:
			--offsetY;
			break;
		case MOVE_UP:
			++offsetY;
			break;
		case '=':
		case '+':
			if(zoom_ < ZoomLevel::MAX_ZOOM)
			{
				++zoom_;
				offsetX = 0;
				offsetY = 0;
			}
			break;
		case '-':
			if(zoom_ > 0)
			{
				--zoom_;
				offsetX = 0;
				offsetY = 0;
			}
			break;
		case SELECT_LEFT:
			selected_ = selectLeft(t, selected_);
			break;
		case SELECT_DOWN:
			selected_ = selectDown(t, selected_);
			break;
		case SELECT_UP:
			selected_ = selectUp(t, selected_);
			break;
		case SELECT_RIGHT:
			selected_ = selectRight(t, selected_);
			break;
		case DISPLAY_INFO:
			t[selected_].displayInfo();
			break;
		default:
			to_draw = false;	/* invalid key, don't redraw */
			break;
		}
	}
	while(input != CLOSE);

}

namespace
{
	void drawTree(const Tree& t, const I16 offsetY, const I16 offsetX)
	{
		erase(); //Clear the screen
		wnoutrefresh(stdscr);

		drawTreeLine(t, focused_, offsetY, offsetX);

		doupdate();
	}

	// return the height of the space occupied by the link
	U8 linkToFirstBorn(I16 startY, I16 startX, bool moreSiblings)
	{
		if (willBeVisible(startY, startX, 3, 1))
		{
			AutoWin pad( newpad(3, 1) );

			waddch(pad, ACS_VLINE);
			mvwaddch(pad, 1, 0,
				 moreSiblings ? ACS_LTEE : ACS_VLINE);
			mvwaddch(pad, 2, 0, ACS_VLINE);

			drawpad(pad, startY, startX);
		}
		return 3;
	}

	U8 linkToChild(I16 startY, I16 startX, I16 endX, bool moreSiblings)
	{
		assert(endX >= startX);
		const I16 len = endX - startX;
		if (willBeVisible(startY, startX, 2, len))
		{
			AutoWin pad( newpad(2, len) );

			for(U16 i = 0; i < len-1; ++i)
				waddch(pad, ACS_HLINE);
			/* whline(pad, '_', len-1); */
			waddch(pad, moreSiblings ? ACS_TTEE : ACS_URCORNER);

			mvwaddch(pad, 1, len-1, ACS_VLINE);

			drawpad(pad, startY, startX);
		}
		return 2;
	}

	I16 drawTreeLine(const Tree& t, const person_id id, I16 drawY, const I16 drawX)
	{
		const Person& person = t[id];

		person_id spouse = t.findSpouse(id);
		vector<person_id> children = t.findChildren(id);

		const size_t numKids = children.size();
		U16 person_w = person.drawAsFocused(drawY, drawX,
						    id == selected_, spouse!=Nobody, numKids!=0, zoom_);

		U16 spouse_w = 0;
		if (spouse != Nobody)
		{
			bool spouse_has_tree = (t.findParent(spouse, M) != Nobody) || (t.findParent(spouse, F) != Nobody);
			U16 spouse_box = t[spouse].drawAsSpouse(drawY, drawX + person_w + spouse_dist_,
								      spouse == selected_, spouse_has_tree, zoom_);
			spouse_w = spouse_dist_ + spouse_box;
		}

		auto childX = drawX;
		if (numKids!=0)
		{
			auto prevX = childX;
			drawY += person_box_height(zoom_);

			auto lnHt = linkToFirstBorn(drawY, childX, numKids > 1); /* Link height */
			childX = drawTreeLine(t, children[0], drawY + lnHt, childX);

			for(size_t j = 1; j < numKids; ++j)
			{
				/* +1 because LTC starts lower then LTFB */
				lnHt = linkToChild(drawY+1, prevX+1, childX+1, j < numKids-1) + 1;

				prevX = childX;
				childX = drawTreeLine(t, children[j], drawY + lnHt, childX);
			}
		}

		return max(drawX + person_w + dist_between_ + spouse_w,
			   childX);
	}



	void displayHelp()
	{
		AutoWin help_tab( newwin(LINES, COLS, 0, 0) );

		waddstr(help_tab, VISUALIZATION_HELP_STR);

		mvwaddstr(help_tab, LINES-1, 0, "Press any key to return.");

		wrefresh(help_tab);
		getch();					/* wait press */

		werase(help_tab);
		wnoutrefresh(help_tab);
	}

	person_id selectLeft(const Tree& t, person_id selected)
	{
		if (t.findCommonAncestor(focused_, selected) != Nobody) //if the selected is part of main tree
		{
			selected = selectLeftSibSpouse(t, selected);
		}
		else
		{
			person_id spouse = t.findSpouse(selected);
			assert(spouse != Nobody);
			selected = spouse;
		}
		return selected;
	}


	person_id selectDown(const Tree& t, person_id selected)
	{
		const auto& curRels = t.findRelations(selected);

		for(const Relation& rel: curRels)
		{
			if (rel.type == Parent)
			{
				selected = rel.id;
				break;
			}
		}
		return selected;
	}

	person_id selectUp(const Tree& t, person_id selected)
	{
		if (t.findCommonAncestor(focused_, selected) != Nobody) //if the selected is part of main tree
		{
			person_id dad = t.findParent(selected, M);
			person_id mom = t.findParent(selected, F);

			if (t.findCommonAncestor(focused_, mom) != Nobody) //if selected's mom is part of main tree
				selected = mom;
			else if (dad != Nobody)
				selected = dad;
		}
		return selected;
	}

	person_id selectRight(const Tree& t, person_id selected)
	{
		if (t.findCommonAncestor(focused_, selected) != Nobody) //if the selected is part of main tree
		{
			if (person_id spouse = t.findSpouse(selected); spouse != Nobody)
			{
				selected = spouse;
			}
			else
			{
				selected = selectRightSib(t, selected);
			}
		}
		else //if spouse of someone in main tree
		{
			person_id spouse = t.findSpouse(selected);
			assert(spouse != Nobody);
			selected = selectRightSib(t, spouse);
		}
		return selected;
	}

	person_id selectLeftSibSpouse(const Tree& t, person_id selected) /* todo: return a value and split */
	{
		person_id sel_par = t.findParent(selected, M);
		if( sel_par == Nobody )
			sel_par = t.findParent(selected, F);
		if( sel_par == Nobody )
			return selected;

		auto siblings = t.findChildren(sel_par);
		person_id left_sib = Nobody;
		for(auto it = siblings.begin()+1; it < siblings.end(); ++it)
		{
			if(*it == selected)
			{
				left_sib = *(it-1);
				person_id sib_spouse = t.findSpouse(left_sib);

				if(sib_spouse != Nobody)
					selected = sib_spouse;
				else
					selected = left_sib;

				break;
			}
		}
		return selected;
	}

	person_id selectRightSib(const Tree& t, person_id selected)
	{
		person_id sel_par = t.findParent(selected, M);
		if( sel_par == Nobody )
			sel_par = t.findParent(selected, F);
		if( sel_par == Nobody )
			return selected;

		auto siblings = t.findChildren(sel_par);
		for(auto it = siblings.begin(); it < siblings.end()-1; ++it)
		{
			if(*it == selected)
			{
				selected = *(it+1);
				break;
			}
		}

		return selected;
	}
}
