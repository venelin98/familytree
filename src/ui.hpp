// Handles keys and displays the tree

#pragma once
#include "common/basicTypes.h"

enum ZoomLevel: U8
{
	NO_NAME,
	FIRST_NAME,
	ALL_NAMES,
	MAX_ZOOM = ALL_NAMES
};
ZoomLevel operator++(ZoomLevel&);
ZoomLevel operator--(ZoomLevel&);

class Tree;
void displayTree(const Tree&);
